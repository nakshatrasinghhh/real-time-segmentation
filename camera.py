import cv2 
from  pixellib.instance import instance_segmentation

segmentation_model = instance_segmentation()
segmentation_model.load_model('mask_rcnn_coco.h5') 

class VideoCamera(object):

    def __init__(self):
        self.video = cv2.VideoCapture(0)
    
    def __del__(self):
        self.video.release() 
    
    def get_frame(self):
        # cap = cv2.VideoCapture(0) 
        while self.video.isOpened():
            success, frame = self.video.read() 

            res = segmentation_model.segmentFrame(frame, show_bboxes=True) 

            image = res[1]             
        
        _, jpeg = cv2.imencode('.jpg', image)
        return jpeg.tobytes() 