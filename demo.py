import cv2
from  pixellib.instance import instance_segmentation

segmentation_model = instance_segmentation()
segmentation_model.load_model('mask_rcnn_coco.h5') 

cap = cv2.VideoCapture(0)
while cap.isOpened():
    success, frame = cap.read() 

    res = segmentation_model.segmentFrame(frame, show_bboxes=True) 

    image = res[1]

    cv2.imshow('Instance Segmentation', image) 

    if cv2.waitKey(10) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows() 